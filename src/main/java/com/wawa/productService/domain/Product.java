// Generated with g9.

package com.wawa.productService.domain;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="product")
public class Product implements Serializable {

    /** Primary key. */
    protected static final String PK = "prodId";

    @Id
    @Column(name="prod_id", unique=true, nullable=false, precision=10)
    private int prodId;
    @Column(name="product_name", length=45)
    private String productName;
    @Column(length=45)
    private String catogory;
    @Column(length=45)
    private String status;
    @Column(nullable=false, length=22)
    private double amount;
  

    /** Default constructor. */
    public Product() {
        super();
    }

    /**
     * Access method for prodId.
     *
     * @return the current value of prodId
     */
    public int getProdId() {
        return prodId;
    }

    /**
     * Setter method for prodId.
     *
     * @param aProdId the new value for prodId
     */
    public void setProdId(int aProdId) {
        prodId = aProdId;
    }

    /**
     * Access method for productName.
     *
     * @return the current value of productName
     */
    public String getProductName() {
        return productName;
    }

    /**
     * Setter method for productName.
     *
     * @param aProductName the new value for productName
     */
    public void setProductName(String aProductName) {
        productName = aProductName;
    }

    /**
     * Access method for catogory.
     *
     * @return the current value of catogory
     */
    public String getCatogory() {
        return catogory;
    }

    /**
     * Setter method for catogory.
     *
     * @param aCatogory the new value for catogory
     */
    public void setCatogory(String aCatogory) {
        catogory = aCatogory;
    }

    /**
     * Access method for status.
     *
     * @return the current value of status
     */
    public String getStatus() {
        return status;
    }

    /**
     * Setter method for status.
     *
     * @param aStatus the new value for status
     */
    public void setStatus(String aStatus) {
        status = aStatus;
    }

    /**
     * Access method for amount.
     *
     * @return the current value of amount
     */
    public double getAmount() {
        return amount;
    }

    /**
     * Setter method for amount.
     *
     * @param aAmount the new value for amount
     */
    public void setAmount(double aAmount) {
        amount = aAmount;
    }

    /**
     * Access method for order.
     *
     * @return the current value of order
     */
 

    /**
     * Compares the key for this instance with another Product.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Product and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Product)) {
            return false;
        }
        Product that = (Product) other;
        if (this.getProdId() != that.getProdId()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Product.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Product)) return false;
        return this.equalKeys(other) && ((Product)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getProdId();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Product |");
        sb.append(" prodId=").append(getProdId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("prodId", Integer.valueOf(getProdId()));
        return ret;
    }

}
