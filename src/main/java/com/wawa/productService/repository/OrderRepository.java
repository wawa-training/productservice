package com.wawa.productService.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wawa.productService.domain.Order;

public interface OrderRepository extends JpaRepository<Order, Integer> {

	
	
	List<Order> findByUserId(int userId);
}
