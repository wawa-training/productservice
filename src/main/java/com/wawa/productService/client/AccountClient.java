package com.wawa.productService.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.wawa.productService.dto.FundTransferReqDto;
import com.wawa.productService.dto.PaymentResDto;


@FeignClient(name="http://wawa.bank/bank-service/account")
public interface AccountClient {
	
	@PostMapping("/payment")
	public PaymentResDto payment(@RequestBody FundTransferReqDto fundTransferDto);

}
