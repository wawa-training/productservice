package com.wawa.productService.dto;

public class PurchaseDto {

	
	public PurchaseDto(int productId) {
		super();
		this.productId = productId;
	}

	public PurchaseDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	private int productId;

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}
	
	
}
