package com.wawa.productService.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wawa.productService.domain.Product;


public interface ProductRepository extends JpaRepository<Product, Integer> {

	
	List<Product> findByProductNameLike(String productName);
	List<Product> findByCatogoryLike(String catogory);
}
