// Generated with g9.

package com.wawa.productService.domain;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_order")
public class Order implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** Primary key. */
    protected static final String PK = "orderId";

    @Id
    @Column(name="order_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int orderId;
    @Column(name="order_amount", nullable=false, length=22)
    private double orderAmount;
    @Column(name="user_id", nullable=false, precision=10)
    private int userId;
    @Column(name="prod_id", precision=10)
    private int prodId;
    @Column(name="transaction_date")
    private Timestamp transactionDate;
    @Column(name="order_status", length=45)
    private String orderStatus;
  
    /** Default constructor. */
    public Order() {
        super();
    }

    /**
     * Access method for orderId.
     *
     * @return the current value of orderId
     */
    public int getOrderId() {
        return orderId;
    }

    /**
     * Setter method for orderId.
     *
     * @param aOrderId the new value for orderId
     */
    public void setOrderId(int aOrderId) {
        orderId = aOrderId;
    }

    /**
     * Access method for orderAmount.
     *
     * @return the current value of orderAmount
     */
    public double getOrderAmount() {
        return orderAmount;
    }

    /**
     * Setter method for orderAmount.
     *
     * @param aOrderAmount the new value for orderAmount
     */
    public void setOrderAmount(double aOrderAmount) {
        orderAmount = aOrderAmount;
    }

    /**
     * Access method for userId.
     *
     * @return the current value of userId
     */
    public int getUserId() {
        return userId;
    }

    /**
     * Setter method for userId.
     *
     * @param aUserId the new value for userId
     */
    public void setUserId(int aUserId) {
        userId = aUserId;
    }

    /**
     * Access method for prodId.
     *
     * @return the current value of prodId
     */
    public int getProdId() {
        return prodId;
    }

    /**
     * Setter method for prodId.
     *
     * @param aProdId the new value for prodId
     */
    public void setProdId(int aProdId) {
        prodId = aProdId;
    }

    /**
     * Access method for product.
     *
     * @return the current value of product
     */
   
    
    

    /**
     * Compares the key for this instance with another Order.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Order and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Order)) {
            return false;
        }
        Order that = (Order) other;
        if (this.getOrderId() != that.getOrderId()) {
            return false;
        }
        return true;
    }

    public Timestamp getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Timestamp transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	/**
     * Compares this instance with another Order.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Order)) return false;
        return this.equalKeys(other) && ((Order)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getOrderId();
        result = 37*result + i;
        return result;
    }

	@Override
	public String toString() {
		return "Order [orderId=" + orderId + ", orderAmount=" + orderAmount + ", userId=" + userId + ", prodId="
				+ prodId + ", transactionDate=" + transactionDate + ", orderStatus=" + orderStatus + "]";
	}

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
   

  

	
}
