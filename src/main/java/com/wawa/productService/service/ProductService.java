package com.wawa.productService.service;


import java.util.List;

import com.wawa.productService.dto.OrderResDto;
import com.wawa.productService.dto.PaymentResDto;
import com.wawa.productService.dto.ProductDto;
import com.wawa.productService.dto.PurchaseReqListDto;

public interface ProductService {

	List<ProductDto> getProducts(String name,String catogory);

	List<ProductDto> getProductByCatogory(String catogory);

	
	PaymentResDto purchase( int userId, PurchaseReqListDto purchaseReqListDto) ;
	
	List<OrderResDto> getOrders(int userId);
	 

}
