package com.wawa.productService.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wawa.productService.client.AccountClient;
import com.wawa.productService.domain.Order;
import com.wawa.productService.domain.Product;
import com.wawa.productService.dto.FundTransferReqDto;
import com.wawa.productService.dto.OrderResDto;
import com.wawa.productService.dto.PaymentResDto;
import com.wawa.productService.dto.ProductDto;
import com.wawa.productService.dto.PurchaseDto;
import com.wawa.productService.dto.PurchaseReqListDto;
import com.wawa.productService.repository.OrderRepository;
import com.wawa.productService.repository.ProductRepository;

@Service
@Transactional
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private AccountClient fundTransferClient;

	@Autowired
	private OrderRepository orderRepository;

	public List<ProductDto> getProducts(String name, String catogory) {

		List<ProductDto> response = null;

		try {
			List<Product> productList = null;
			if (StringUtils.isNotEmpty(name)) {
				productList = productRepository.findByProductNameLike("%" + name + "%");
			} else {
				productList = productRepository.findByCatogoryLike("%" + catogory + "%");
			}
			response = new ArrayList<ProductDto>();
			for (Product product : productList) {
				ProductDto dto = new ProductDto();
				BeanUtils.copyProperties(product, dto);
				response.add(dto);
			}
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
		System.out.println("Response :" + response);
		return response;
	}

	public List<ProductDto> getProductByCatogory(String catogory) {

		List<ProductDto> response = null;

		try {

			List<Product> productList = productRepository.findByCatogoryLike("%" + catogory + "%");
			System.out.println("DB Response productList :" + productList);
			response = new ArrayList<ProductDto>();
			for (Product product : productList) {
				ProductDto dto = new ProductDto();
				BeanUtils.copyProperties(product, dto);
				response.add(dto);
			}

		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
		System.out.println("Response :" + response);
		return response;
	}

	public PaymentResDto purchase(int userId, PurchaseReqListDto purchaseReqListDto) {
		PaymentResDto paymentResDto = null;
		try {
			double totalAmount = 0;
			Optional<Product> product = null;
			List<Order> orders = new ArrayList<Order>();
			Date currentDate = new Date();
			Order order = null;
			for (PurchaseDto purchaseDto : purchaseReqListDto.getPurchaseDtos()) {
				product = productRepository.findById(purchaseDto.getProductId());
				if (product.isPresent()) {
					totalAmount = product.get().getAmount() + totalAmount;
					order = new Order();
					order.setOrderAmount(product.get().getAmount());
					order.setUserId(userId);
					order.setProdId(product.get().getProdId());
					order.setTransactionDate(new Timestamp(currentDate.getTime()));

					orders.add(order);
				}

			}

			FundTransferReqDto fundTransferReqDto = new FundTransferReqDto(totalAmount,
					purchaseReqListDto.getAccountFrom(), null, "Purchasing product");
			paymentResDto = fundTransferClient.payment(fundTransferReqDto);
			if (paymentResDto.getStatus().equals("SUCCESS")) {
				System.out.println("Orders::" + orders);
				for (Order order2 : orders) {
					order2.setOrderStatus("SUCCESS");
					orderRepository.save(order2);
				}
			} else {
				for (Order order2 : orders) {
					order2.setOrderStatus("FAILED");
					orderRepository.save(order2);
				}
			}

		} catch (Exception e) {
			paymentResDto = new PaymentResDto(0, "FAILED");
		}
		return paymentResDto;
	}

	public List<OrderResDto> getOrders(int userId) {

		List<OrderResDto> list = new ArrayList<OrderResDto>();

		try {
			List<Order> dbOrders = orderRepository.findByUserId(userId);
			if (dbOrders != null && !dbOrders.isEmpty()) {
				OrderResDto orderResDto = null;
				for (Order order : dbOrders) {
					orderResDto = new OrderResDto();
					BeanUtils.copyProperties(order, orderResDto);
					Optional<Product> product = productRepository.findById(order.getProdId());
					orderResDto.setProductName(product.isPresent() ? product.get().getProductName() : "");
					list.add(orderResDto);
				}

			}

		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
		return list;
	}

}
