package com.wawa.productService.dto;

import java.util.List;

public class PurchaseReqListDto {

	
	
	public PurchaseReqListDto(List<PurchaseDto> purchaseDtos) {
		super();
		this.purchaseDtos = purchaseDtos;
	}

	
	
	public PurchaseReqListDto() {
		super();
		// TODO Auto-generated constructor stub
	}


    private String accountFrom;
    
    
	public String getAccountFrom() {
		return accountFrom;
	}



	public void setAccountFrom(String accountFrom) {
		this.accountFrom = accountFrom;
	}



	


	private List<PurchaseDto> purchaseDtos ;

	public List<PurchaseDto> getPurchaseDtos() {
		return purchaseDtos;
	}

	public void setPurchaseDtos(List<PurchaseDto> purchaseDtos) {
		this.purchaseDtos = purchaseDtos;
	}
	
	
}
