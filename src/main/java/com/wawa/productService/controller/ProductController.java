package com.wawa.productService.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wawa.productService.dto.OrderResDto;
import com.wawa.productService.dto.PaymentResDto;
import com.wawa.productService.dto.ProductDto;
import com.wawa.productService.dto.PurchaseReqListDto;
import com.wawa.productService.service.ProductService;

@RestController
@RequestMapping("/product")
public class ProductController {

	@Autowired
	private ProductService productService;
	
	@GetMapping("/search")
	public List<ProductDto> getProducts(@RequestParam(required=false) String name,@RequestParam(required=false) String catogory) {
		 return productService.getProducts(name,catogory);
	}
				
	@PostMapping("/purchase/{userId}")
	public PaymentResDto purchase(@PathVariable int userId, @RequestBody PurchaseReqListDto purchaseReqListDto) {
		
		return productService.purchase(userId, purchaseReqListDto);
	}
	
	@GetMapping("/order/{userId}")
	public List<OrderResDto> getOrders(@PathVariable int userId) {
		 return productService.getOrders(userId);
	}
	
	
}
