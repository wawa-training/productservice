package com.wawa.productService.dto;


public class ProductDto {

	
	
	    public ProductDto(int prodId, String productName, String catogory, String status, double amount) {
		super();
		this.prodId = prodId;
		this.productName = productName;
		this.catogory = catogory;
		this.status = status;
		this.amount = amount;
	}
	    
	    
	    
		public ProductDto() {
			super();
			// TODO Auto-generated constructor stub
		}



		private int prodId;
	    private String productName;
	    private String catogory;
	    private String status;
	    private double amount;
		public int getProdId() {
			return prodId;
		}



		public void setProdId(int prodId) {
			this.prodId = prodId;
		}



		public String getProductName() {
			return productName;
		}



		public void setProductName(String productName) {
			this.productName = productName;
		}



		public String getCatogory() {
			return catogory;
		}



		public void setCatogory(String catogory) {
			this.catogory = catogory;
		}



		public String getStatus() {
			return status;
		}



		public void setStatus(String status) {
			this.status = status;
		}



		public double getAmount() {
			return amount;
		}



		public void setAmount(double amount) {
			this.amount = amount;
		}
	
	    
	    
}
